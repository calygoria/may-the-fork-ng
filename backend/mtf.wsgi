#!/usr/bin/python3
import sys

# add your project directory to the sys.path
project_home = u'/var/www/mtf/backend/'
if project_home not in sys.path:
     sys.path = [project_home] + sys.path

from app import app as application
