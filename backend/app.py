#!/usr/bin/python3
import os

from flask import Flask, Blueprint
from flask_restx import Api
from peewee import SqliteDatabase

APP_ROOT = os.path.dirname(os.path.realpath(__file__))
DATABASE = os.path.join(APP_ROOT, 'may-the-fork.db')
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'static')

# Init API
api_v1 = Blueprint("api", __name__)

api = Api(api_v1,
    version="1.0",
    title="MayTheFork API",
    security='Basic Auth',
    authorizations={
        'Basic Auth': {
            'type': 'basic',
            'in': 'header',
            'name': 'Authorization'
        }
    }
)

# Init DB
db = SqliteDatabase(DATABASE)

app = Flask(__name__)
app.config.from_object(__name__)

@app.before_request
def before_request():
    db.connect(reuse_if_open=True)

@app.after_request
def after_request(response):
    db.close()
    return response

app.register_blueprint(api_v1)

from may_the_fork.api import recipe_ns, user_ns
api.add_namespace(recipe_ns)
api.add_namespace(user_ns)

from may_the_fork.models import User, Recipe
db.create_tables([Recipe, User], safe=True)


if __name__ == '__main__':
    app.run(debug=True)
