import datetime

from peewee import *
from playhouse.sqlite_ext import JSONField

from app import db


class BaseModel(Model):
    class Meta:
        database = db


class Recipe(BaseModel):
    name = TextField()
    type = TextField()
    description = TextField(null=True)
    createdDate = DateTimeField(default=datetime.datetime.now)
    preparationTime = IntegerField()
    cookingTime = IntegerField()
    portions = IntegerField()
    difficulty = IntegerField()
    ingredients = JSONField()
    steps = TextField()
    pictureName = TextField()


class User(BaseModel):
    username = TextField(unique=True)
    password = TextField()
    salt = TextField()
