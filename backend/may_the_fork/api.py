from base64 import b64decode
import binascii
import hashlib
from http import HTTPStatus
import os
import logging  # TODO

from flask import request
from flask_restx import Namespace, Resource, fields
from peewee import DoesNotExist
from playhouse.shortcuts import model_to_dict
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage

recipe_ns = Namespace("recipe", description="Recipe operations")
user_ns = Namespace("user", description="User operations")

from app import UPLOAD_FOLDER, api
from may_the_fork.models import Recipe, User

logger = logging.getLogger()
# logging.basicConfig(filename="/var/log/mtf/api.log", level=logging.DEBUG)

def encrypt_password(password, salt):
    return binascii.hexlify(hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000, 32)).decode("utf-8")

def check_authentication(f):
    def wrapper_func(*args, **kwargs):
        try:
            authorization = request.headers.get('Authorization')[6:]
            username, password = b64decode(authorization).decode('utf-8').split(':')
            user = User.get(User.username == username)
            salt = binascii.unhexlify(user.salt.encode("utf-8"))
            if user.password != encrypt_password(password, salt):
                raise Exception('Wrong password provided')
        except Exception:
            return None, HTTPStatus.UNAUTHORIZED
        return f(*args, **kwargs)

    return wrapper_func


# User endpoint
user_parser = api.parser()
user_parser.add_argument('username', type=str, required=True, nullable=False)
user_parser.add_argument('password', type=str, required=True, nullable=False)

# @user_ns.route('/')
# class UserResource(Resource):

#     def get(self):
#         users = User.select()
#         return [model_to_dict(user) for user in users], HTTPStatus.OK

#     @api.expect(user_parser, validate=True)
#     def post(self):
#         args = user_parser.parse_args()
#         username = args.pop('username')
#         password = args.pop('password')
        
#         if User.select().where(User.username == username):
#             return "Username already in use", HTTPStatus.BAD_REQUEST

#         salt = os.urandom(32)
#         user = User(
#             username=username,
#             password=encrypt_password(password, salt),
#             salt=binascii.hexlify(salt).decode("utf-8"))
#         user.save()

#         return model_to_dict(user), HTTPStatus.CREATED

@user_ns.route('/authenticate')
class AuthenticateUserResource(Resource):

    @api.expect(user_parser, validate=True)
    def post(self):
        args = user_parser.parse_args()
        username = args.pop('username')
        password = args.pop('password')
        try:
            user = User.get(User.username == username)
        except DoesNotExist:
            return None, HTTPStatus.BAD_REQUEST

        salt = binascii.unhexlify(user.salt.encode("utf-8"))
        if user.password != encrypt_password(password, salt):
            return None, HTTPStatus.BAD_REQUEST

        return None, HTTPStatus.ACCEPTED

# Recipe endpoint
file_parser = api.parser()
file_parser.add_argument('file', location='files', type=FileStorage, required=True)


class NullableString(fields.String):
    __schema_type__ = ['string', 'null']
    __schema_example__ = 'nullable string'

ingredient_fields = recipe_ns.model('Ingredient', {
    'name': fields.String(required=True),
    'amount': fields.Float(required=True),
    'measuringUnit': NullableString
})

recipe_fields = {
    'name': fields.String(required=True),
    'type': fields.String(required=True),
    'description': NullableString,
    'preparationTime': fields.Integer(required=True),
    'cookingTime': fields.Integer(required=True),
    'portions': fields.Integer(required=True),
    'difficulty': fields.String(required=True),
    'ingredients': fields.List(fields.Nested(ingredient_fields, skip_none=True), required=True),
    'steps': fields.String(required=True),
    'pictureName': fields.String(required=True)
}

recipe_fields_post = recipe_ns.model('RecipePost', recipe_fields)
recipe_fields_put = recipe_ns.model('RecipePut', {**{'id': fields.Integer}, **recipe_fields})
recipe_fields_get = recipe_ns.model('RecipeGet', {**{'id': fields.Integer, 'createdDate': fields.DateTime}, **recipe_fields})

@recipe_ns.route('/')
class RecipesResource(Resource):

    @api.marshal_with(recipe_fields_get, as_list=True, skip_none=True)
    def get(self):
        offset = request.args.get('offset', 0)
        limit = request.args.get('limit', -1)
        sorting_params = []
        for sort_param in request.args.get('sort', '').split(','):
            if sort_param.startswith('-'):
                if hasattr(Recipe, sort_param[1:]):
                    sorting_params.append(getattr(Recipe, sort_param[1:]).desc())
            else:
                if hasattr(Recipe, sort_param):
                    sorting_params.append(getattr(Recipe, sort_param).asc())
        recipes = Recipe.select().order_by(*sorting_params).offset(offset).limit(limit)
        return [model_to_dict(recipe) for recipe in recipes], HTTPStatus.OK

    @api.expect(recipe_fields_post, validate=True)
    @api.marshal_with(recipe_fields_get, skip_none=True)
    @check_authentication
    def post(self):
        recipe = Recipe(**api.payload)
        recipe.save()

        return model_to_dict(recipe), HTTPStatus.CREATED


@recipe_ns.route('/<int:id>')
@recipe_ns.doc(params={'id': 'Recipe ID'}, responses={404: "Recipe not found"})
class RecipeResource(Resource):

    @api.marshal_with(recipe_fields_get, skip_none=True)
    def get(self, id):
        try:
            return model_to_dict(Recipe.get_by_id(id))
        except DoesNotExist:
            return None, HTTPStatus.NOT_FOUND

    @api.expect(recipe_fields_put)
    @api.marshal_with(recipe_fields_get, skip_none=True)
    @check_authentication
    def put(self, id):
        try:
            existing_recipe = Recipe.get_by_id(id)
        except DoesNotExist:
            return None, HTTPStatus.NOT_FOUND

        recipe = Recipe(**api.payload)
        recipe.createdDate = existing_recipe.createdDate

        if existing_recipe.pictureName != recipe.pictureName:
            delete_picture(existing_recipe.pictureName)

        recipe.save()

        return model_to_dict(recipe), HTTPStatus.OK

    @check_authentication
    def delete(self, id):
        try:
            recipe = Recipe.get_by_id(id)
        except DoesNotExist:
            return None, HTTPStatus.NOT_FOUND

        delete_picture(recipe.pictureName)

        Recipe.delete_by_id(id)


@recipe_ns.route('/file')
class FileResource(Resource):

    @api.expect(file_parser, validate=True)
    @check_authentication
    def post(self):
        file: FileStorage = file_parser.parse_args().get('file', None)
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            return filename, HTTPStatus.CREATED
        return None, HTTPStatus.NOT_FOUND


def delete_picture(picture_name: str):
    picture_path = os.path.join(UPLOAD_FOLDER, picture_name)
    pictures = [recipe.pictureName for recipe in Recipe.select() if recipe.pictureName == picture_name]
    if len(pictures) == 1 and os.path.exists(picture_path):
        os.remove(picture_path)
