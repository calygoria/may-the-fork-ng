from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="may-the-fork-backend", # Replace with your own username
    version="0.0.1",
    author="Calygoria",
    author_email="contact@calygoria.com",
    description="An api to handle recipes",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/calygoria/may-the-fork-ng",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(),
    python_requires=">=3.6",
    install_requires=[
        'flask-restx',
        'peewee'
    ],
    entry_points={
        'console_scripts': [
            'may-the-fork = app:main'
        ]
    }
)
