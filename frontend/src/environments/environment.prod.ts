export const environment = {
  production: true,
  serverApiUrl: 'https://maythefork.calygoria.fr/api/v1'
};
