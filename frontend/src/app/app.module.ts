import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fontAwesomeIcons } from './utils/icons';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { RecipeListComponent } from './recipe/recipe-list.component';
import { RecipeUpdateModalComponent } from './recipe/recipe-update-modal.component';
import { RecipeDetailComponent } from './recipe/recipe-detail.component';
import { RecipeDeleteModalComponent } from './recipe/recipe-delete-modal.component';
import { AuthInterceptor } from './connection/auth-interceptor';
import { ConnectionModalComponent } from './connection/connection-modal.component';
import { IsAuthenticatedDirective } from './connection/is-authenticated.directive';

@NgModule({
  declarations: [
    AppComponent,
    ConnectionModalComponent,
    RecipeListComponent,
    RecipeUpdateModalComponent,
    RecipeDetailComponent,
    RecipeDeleteModalComponent,
    IsAuthenticatedDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    FontAwesomeModule,
    AngularEditorModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(iconLibrary: FaIconLibrary) {
    iconLibrary.addIcons(...fontAwesomeIcons)
  };
}
