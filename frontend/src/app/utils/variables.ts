export const ALPHABETIC_FORMAT = '^[a-zA-Z \\-,\'\.\u00C0-\u00FF]*$';
export const UNIT_FORMAT = '^[a-zA-Z \-,\.\(\)\u00C0-\u00FF]*$';
export const INTEGER_FORMAT = '^[0-9]*$';
export const FLOAT_FORMAT = '^[0-9.]*$';