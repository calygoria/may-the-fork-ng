import {
    faClock,
    faEye,
    faEyeSlash,
    faPaperPlane,
    faPen,
    faPlus,
    faSave,
    faStar,
    faTrash,
    faUser,
    faXmark,
} from '@fortawesome/free-solid-svg-icons';

import {
    faStar as faEmptyStar
} from '@fortawesome/free-regular-svg-icons';

export const fontAwesomeIcons = [
    faClock,
    faEmptyStar,
    faEye,
    faEyeSlash,
    faPaperPlane,
    faPen,
    faPlus,
    faSave,
    faStar,
    faTrash,
    faUser,
    faXmark,
];
