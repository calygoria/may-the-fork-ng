import { Component, Input, OnInit } from "@angular/core";
import { AbstractControl, FormArray, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { EventManager } from "../utils/event-manager.service";
import { ALPHABETIC_FORMAT, FLOAT_FORMAT, INTEGER_FORMAT, UNIT_FORMAT } from "../utils/variables";
import { Ingredient, Recipe } from "./recipe.model";
import { RecipeService } from "./recipe.service";

@Component({
  templateUrl: './recipe-update-modal.component.html',
  styleUrls: ['./recipe-update-modal.component.scss']
})
export class RecipeUpdateModalComponent implements OnInit {
  @Input() currentRecipe?: Recipe;

  measuringUnits = ['g', 'cL', 'c.à.c', 'c.à.s', 'gousse(s)', 'pincée(s)', 'sachet', 'tranche(s)'];

  recipeForm = this.fb.group({
    name: [null, [Validators.required]],
    type: ['Plat', [Validators.required]],
    description: [],
    preparationTime: [30, [Validators.required, Validators.pattern(INTEGER_FORMAT), Validators.min(0), Validators.max(600)]],
    cookingTime: [30, [Validators.required, Validators.pattern(INTEGER_FORMAT), Validators.min(0), Validators.max(600)]],
    portions: [8, [Validators.required, Validators.pattern(INTEGER_FORMAT), Validators.min(1), Validators.max(50)]],
    difficulty: ['2', [Validators.required, Validators.pattern(INTEGER_FORMAT), Validators.min(1), Validators.max(5)]],
    ingredients: this.fb.array([]),
    steps: [null, [Validators.required]],
    picture: [null, [Validators.required]]
  });

  fileToUpload: File | null = null;
  isSaving = false;

  update = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    fonts: [],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['fontName', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'fontSize', 'textColor', 'backgroundColor',
      'insertImage', 'insertVideo', 'insertHorizontalRule']
    ]
  };

  constructor(private fb: FormBuilder, public activeModal: NgbActiveModal, private recipeService: RecipeService, private eventManager: EventManager, private router: Router) { }

  ngOnInit(): void {
    this.update = !!this.currentRecipe;
    if (this.update) {
      this.updateFormFromRecipe();
    } else {
      this.addIngredient();
    }
  }

  get ingredients(): FormArray {
    return <FormArray>this.recipeForm.get("ingredients");
  }

  handleFileInput(eventTarget: EventTarget | null): void {
    if (eventTarget) {
      const htmlElement = <HTMLInputElement>eventTarget;
      this.fileToUpload = htmlElement.files![0];
    }
  }

  save(): void {
    this.isSaving = true;

    // Creation or update of the picture
    if (!this.update || (this.fileToUpload?.name && this.currentRecipe?.pictureName !== this.fileToUpload?.name)) {
      const formData = new FormData()
      formData.append('file', this.fileToUpload!, this.fileToUpload!.name)
      this.recipeService.uploadPicture(formData).subscribe({
        next: () => {
          this.saveRecipe();
        },
        error: (err) => {
          console.error(err);
          this.isSaving = false;
        }
      });
    } else {
      this.saveRecipe();
    }
  }

  addIngredient(ingredient?: Ingredient): void {
    this.ingredients.push(
      this.fb.group({
        name: [ingredient?.name ?? null, [Validators.required, Validators.pattern(ALPHABETIC_FORMAT)]],
        amount: [ingredient?.amount ?? null, [Validators.required, Validators.pattern(FLOAT_FORMAT)]],
        measuringUnit: [ingredient?.measuringUnit ?? null, [Validators.pattern(UNIT_FORMAT)]]
      })
    );
  }

  deleteIngredient(index: number): void {
    this.ingredients.removeAt(index);
  }

  selectMeasure(control: AbstractControl, measure: string): void {
    control.setValue(measure);
  }

  close(): void {
    this.activeModal.dismiss();
  }

  private saveRecipe(): void {
    const recipe = this.createRecipeFromForm();
    const recipeObs = this.update ? this.recipeService.update(recipe) : this.recipeService.create(recipe);
    recipeObs.subscribe({
      next: (res) => {
        if (!this.update) {
          this.router.navigate([`/recette/${res.id}`]);
        }
        this.eventManager.broadcast('recipeListModification');
        this.activeModal.close(res);
      },
      error: (error) => {
        console.error(error);
        this.isSaving = false;
      }
    });
  }

  private createRecipeFromForm(): Recipe {
    return {
      ... new Recipe(
        this.recipeForm.get('name')!.value,
        this.recipeForm.get('type')!.value,
        this.recipeForm.get('preparationTime')!.value,
        this.recipeForm.get('cookingTime')!.value,
        this.recipeForm.get('portions')!.value,
        this.recipeForm.get('difficulty')!.value,
        this.recipeForm.get('ingredients')!.value,
        this.recipeForm.get('steps')!.value,
        this.fileToUpload?.name ?? this.currentRecipe!.pictureName,
        this.recipeForm.get('description')?.value,
        this.currentRecipe?.id
      ),
    }
  }

  private updateFormFromRecipe(): void {
    this.recipeForm.patchValue({
      name: this.currentRecipe!.name,
      description: this.currentRecipe!.description,
      type: this.currentRecipe!.type,
      preparationTime: this.currentRecipe!.preparationTime,
      cookingTime: this.currentRecipe!.cookingTime,
      portions: this.currentRecipe!.portions,
      difficulty: this.currentRecipe!.difficulty,
      ingredients: this.currentRecipe!.ingredients,
      steps: this.currentRecipe!.steps
    })
    this.currentRecipe!.ingredients.forEach(ingredient => {
      this.addIngredient(ingredient);
    })
    this.recipeForm.get('picture')?.setValidators(null);
  }
}