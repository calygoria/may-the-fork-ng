import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { environment } from "src/environments/environment";
import { RecipeDeleteModalComponent } from "./recipe-delete-modal.component";
import { RecipeUpdateModalComponent } from "./recipe-update-modal.component";
import { Recipe } from "./recipe.model";
import { copy } from "../utils/copy";

@Component({
  selector: 'recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RecipeDetailComponent implements OnInit {
  recipe?: Recipe;

  constructor(private route: ActivatedRoute, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.route.data.subscribe(({ recipe }) => {
      this.recipe = recipe;
    });
  }

  editRecipe(): void {
    const modalRef = this.modalService.open(RecipeUpdateModalComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.currentRecipe = this.recipe;
    modalRef.result.then(
      recipe => { this.recipe = recipe; },
      () => {}
    );
  }

  deleteRecipe(): void {
    const modalRef = this.modalService.open(RecipeDeleteModalComponent);
    modalRef.componentInstance.recipe = this.recipe;
  }

  copyIngredient(ingredientName: string): void {
    copy(ingredientName)
  }

  getImageUrl(imageName: string): string {
    return `${environment.serverApiUrl}/static/${imageName}`
  }
}