export enum RecipeType {
    ENTREE = 'Entrée',
    PLAT = 'Plat',
    DESSERT = 'Dessert'
}

export class Ingredient {
    constructor(
        public name: string,
        public amount: number,
        public measuringUnit: string
    ) { }
}

export class Recipe {
    constructor(
        public name: string,
        public type: RecipeType,
        public preparationTime: number,
        public cookingTime: number,
        public portions: number,
        public difficulty: string,
        public ingredients: Ingredient[],
        public steps: string,
        public pictureName: string,
        public description?: string,
        public id?: number,
    ) { }
}