import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { environment } from "src/environments/environment";
import { EventManager } from "../utils/event-manager.service";
import { Recipe } from "./recipe.model";
import { RecipeService } from "./recipe.service";

@Component({
  selector: 'recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {
  recipes?: Recipe[];

  recipeModificationListener: Subscription;

  title: string = '';
  sort: string = 'createdDate';
  offset: number = 0;
  limit: number = -1;

  constructor(private activatedRoute: ActivatedRoute, private recipeService: RecipeService, private eventManager: EventManager) {
    this.recipeModificationListener = this.eventManager.subscribe('recipeListModification', () => this.loadPage());
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.title = data['title']
      this.sort = data['sort']
      this.offset = data['offset']
      this.limit = data['limit']
      this.loadPage();
    });
  }

  ngOnDestroy(): void {
    this.eventManager.destroy(this.recipeModificationListener);
  }

  loadPage(): void {
    this.recipeService.getAll({
      sort: this.sort,
      offset: this.offset,
      limit: this.limit
    }).subscribe(res => {
      this.recipes = res;
    })
  }

  getImageUrl(imageName: string): string {
    return `${environment.serverApiUrl}/static/${imageName}`
  }
}