import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { EventManager } from "../utils/event-manager.service";
import { Recipe } from "./recipe.model";
import { RecipeService } from "./recipe.service";

@Component({
  templateUrl: './recipe-delete-modal.component.html'
})
export class RecipeDeleteModalComponent {
  @Input() recipe!: Recipe;

  constructor(
    public activeModal: NgbActiveModal,
    private recipeService: RecipeService,
    private eventManager: EventManager,
    private router: Router
  ) { }

  confirmDelete(): void {
    this.recipeService.delete(this.recipe.id!).subscribe({
      next: () => {
        this.eventManager.broadcast('recipeListModification');
        this.activeModal.close();
        this.router.navigate(['/']);
      },
      error: (error) => {
        console.error(error);
      }
    })
  }

  close(): void {
    this.activeModal.dismiss();
  }
}