import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Recipe } from "./recipe.model";
import { environment } from "src/environments/environment";

@Injectable({ providedIn: 'root' })
export class RecipeService {
    private recipeUrl = `${environment.serverApiUrl}/recipe/`;

    constructor(private http: HttpClient) { }

    getById(id: number): Observable<Recipe> {
        return this.http.get<Recipe>(`${this.recipeUrl}${id}`);
    }

    getAll(params?: { [id: string]: any }): Observable<Recipe[]> {
        return this.http.get<Recipe[]>(this.recipeUrl, { params });
    }

    uploadPicture(file: FormData): Observable<{}> {
        return this.http.post(`${this.recipeUrl}file`, file);
    }

    create(recipe: Recipe): Observable<Recipe> {
        return this.http.post<Recipe>(this.recipeUrl, recipe);
    }

    update(recipe: Recipe): Observable<Recipe> {
        return this.http.put<Recipe>(`${this.recipeUrl}${recipe.id}`, recipe);
    }

    delete(id: number): Observable<{}> {
        return this.http.delete(`${this.recipeUrl}/${id}`);
    }
}