import { Component } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "./authentication.service";

@Component({
    templateUrl: './connection-modal.component.html',
    styleUrls: ['./connection-modal.component.scss']
})
export class ConnectionModalComponent {

    connectionForm = this.fb.group({
        username: [null, [Validators.required]],
        password: [null, [Validators.required]]
    });

    revealPassword = false;
    isConnecting = false;

    credentialsError = false;

    constructor(
        public activeModal: NgbActiveModal,
        private authenticationService: AuthenticationService,
        protected fb: FormBuilder
    ) { }

    togglePassword(): void {
        this.revealPassword = !this.revealPassword;
    }

    connect(): void {
        this.isConnecting = true;
        this.credentialsError = false;
        this.authenticationService.login(
            this.connectionForm.get('username')!.value,
            this.connectionForm.get('password')!.value
        ).subscribe({
            next: () => { this.activeModal.close(); },
            error: () => {
                this.isConnecting = false;
                this.credentialsError = true;
            }
        })
    }

    close(): void {
        this.activeModal.dismiss();
    }

    closeAlert(): void {
        this.credentialsError = false;
    }
}