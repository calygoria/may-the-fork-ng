import { Directive, Input, OnDestroy, TemplateRef, ViewContainerRef } from "@angular/core";
import { Subject, takeUntil } from "rxjs";
import { AuthenticationService } from "./authentication.service";

@Directive({
    selector: '[isAuthenticated]',
})
export class IsAuthenticatedDirective implements OnDestroy {
    authenticated = false;
    private readonly destroy$ = new Subject<void>();
    
    constructor(private authenticationService: AuthenticationService, private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}
    
    @Input()
    set isAuthenticated(value: boolean) {
        this.authenticated = value;
        this.updateView();
        // Get notified each time authentication state changes.
        this.authenticationService.currentUser.pipe(takeUntil(this.destroy$)).subscribe(() => {
            this.updateView();
        });
    }
    
    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
    
    private updateView(): void {
        const displayContent = (this.authenticated && this.authenticationService.isAuthenticated()) || (!this.authenticated && !this.authenticationService.isAuthenticated());
        this.viewContainerRef.clear();
        if (displayContent) {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
    }
}