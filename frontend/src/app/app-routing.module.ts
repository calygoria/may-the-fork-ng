import { Injectable, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, Routes } from '@angular/router';
import { Observable, of, catchError } from 'rxjs';
import { PagenotfoundComponent } from './page-not-found/page-not-found.component';
import { RecipeDetailComponent } from './recipe/recipe-detail.component';
import { RecipeListComponent } from './recipe/recipe-list.component';
import { Recipe } from './recipe/recipe.model';
import { RecipeService } from './recipe/recipe.service';

@Injectable({ providedIn: 'root' })
export class RecipeResolve implements Resolve<Recipe> {
  constructor(private service: RecipeService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Recipe> {
    const id = route.params['recipeId'];
    if (id) {
      return this.service.getById(id).pipe(
        catchError(() => {
          this.router.navigate(["/"]);
          return of();
        })
      );
    }
    return of();
  }
}

const routes: Routes = [
  {
    path: '',
    component: RecipeListComponent,
    data: {
      title: 'Les dernières recettes ajoutées',
      sort: '-createdDate',
      offset: 0,
      limit: 6
    }
  },
  {
    path: 'toutes-les-recettes',
    component: RecipeListComponent,
    data: {
      title: 'Toutes les recettes',
      sort: 'createdDate',
      offset: 0,
      limit: -1
    }
  },
  {
    path: 'recette/:recipeId',
    component: RecipeDetailComponent,
    resolve: {
      recipe: RecipeResolve,
    }
  },
  {
    path: 'fr',
    redirectTo: ''
  },
  {
    path: '**',
    pathMatch: 'full',
    component: PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
