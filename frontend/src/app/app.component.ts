import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { AuthenticationService } from './connection/authentication.service';
import { ConnectionModalComponent } from './connection/connection-modal.component';
import { RecipeUpdateModalComponent } from './recipe/recipe-update-modal.component';
import { Recipe } from './recipe/recipe.model';
import { RecipeService } from './recipe/recipe.service';
import { EventManager } from './utils/event-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  recipes: Recipe[] = [];
  isCollapsed = true;

  recipeModificationListener: Subscription;

  constructor(
    private modalService: NgbModal,
    private recipeService: RecipeService,
    private router: Router,
    private eventManager: EventManager,
    private authenticationService: AuthenticationService,
  ){
    this.recipeModificationListener = this.eventManager.subscribe('recipeListModification', () => this.loadRecipes());
  }

  ngOnInit(): void {
    this.loadRecipes();
  }

  ngOnDestroy(): void {
    this.eventManager.destroy(this.recipeModificationListener);
  }

  addRecipe() {
    this.modalService.open(RecipeUpdateModalComponent, { size: 'lg', backdrop: 'static' });
  }

  goToRecipe(recipe?: Recipe): void {
    if (recipe) {
      this.goToPage(`/recette/${recipe.id}`);
    } else {
      this.goToPage();
    }
    this.isCollapsed = true;
  }

  goToPage(page?: string): void {
    this.router.navigate([page ?? '/']);
    this.isCollapsed = true;
  }

  goToRandomRecipe(): void {
    this.goToRecipe(this.recipes[Math.floor(Math.random() * this.recipes.length)]);
  }

  connect(): void {
    this.modalService.open(ConnectionModalComponent, { backdrop: 'static' });
  }

  disconnect(): void {
    this.authenticationService.logout();
  }

  private loadRecipes(): void {
    this.recipeService.getAll().subscribe(res => {
      this.recipes = res;
    })
  }
}
